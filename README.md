# Paysera Onscreen

## Description
Paysera Onscreen is Magento Paysera complimentary module that replaces payment method gateway with JavaScript activated function that connects user to Paysera enabled JavaScript method to pay via card

## Requirements:
 * Magento 1.9 (tested on Magento 1.9.2.1)
 * Paysera module
 * Onepage checkout (tho by editing the `app/design/frontend/base/default/layout/balticode/onscreencheckout.xml` it's likely it would work on some other checkouts)

## Setup:
 1. Copy app and js contents and paste them in appropriate directories
 2. Enable module in admin panel
 3. (optional) In configuration menu add JavaScript to remove elements in checkout such as loaders (basically everything that would interfere with user after window close or unsuccessful launch)
 4. enjoy

## Changelog

#### v 0.1.15
 * __Added:__ When "Paysera" service is unavailable order is automatically canceled (if redirect option is selected user is redirected to cart)

#### v 0.1.14
 * __Added:__ function checkCard in helper
 * __Added:__ buildRequest function as a medium to proccess data before sending to generateDataSign
 * __Added:__ constructor for Request model that includes paysera WebToPay classes
 * __Changed:__ controllers have been changed to require new models
 * __Moved:__ payValidate and generateDataSign to Requests model
 * __Updated:__ checkActive function by removing cases and focusing on checking weather a module is active or not
 * __Updated:__ variable naming to be more meaningful

#### v 0.1.13
 * __Changed:__ cancellation message type from error to notice
 * __Fixed:__ order failing to cancel
 * __Updated:__ cancellation message to remove grammar

#### v 0.1.12
 * Bug fixes

#### v 0.1.11
 * __Added:__ support for multiple JavaScript lines
 * __Changed:__ json controller function names so it would be more appropriate
 * __Removed:__ unneeded cases in JavaScript
 * __Updated:__ helper handshake check to actually check for content inside
 * __Updated:__ json cancel action for ajax recognition
 * __Updated:__ internal model function comment block for easier recognition

#### v 0.1.10
 * __Fixed:__ validation would only return no such order!
 * __Removed:__ internalModel functions
 * __Removed:__ getHandshake function
 * __Moved:__ payseras script to load from xml file, not from js file
 * __Updated:__ json controller
 * __Updated:__ index controller
 * __Updated:__ xml files

#### v 0.1.9
 * __Fixed:__ bug that would cause an error if user where to try access onscreen/index/success/ directly
 * __Moved:__ active checks to helper
 * __Removed:__ unnecessary variables
 * General Cleanup
