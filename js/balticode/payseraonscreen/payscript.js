/**
 * Function for when the paysera service is unavailable
 * Does the same thing as cancel, just different message if a setting
 * is set to redirect
 * 
 * @param bool redirectOnCancel 
 * @param object url 
 * @param string functionset 
 */
function failedToConnect(redirectOnCancel, url, functionset) {
    if (redirectOnCancel) {
        window.location.href = url.fail;
    } else {
        eval(functionset);
        jQuery.ajax({
            url: url.jsredirectOnCancel,
            method: 'POST',
            cache: false,
            dataType: 'json',
            data: { handshake: redirectOnCancel },
            error: function () {
                window.location.href = url.fail;
            }
        });
    }
}
/**
 * Cancels order by reserved order id
 * 
 * @param bool redirectOnCancel 
 * @param string url 
 */
function cancelOnscreen(redirectOnCancel, url) {
    if (redirectOnCancel) {
        window.location.href = url.cancel;
    } else {
        jQuery.ajax({
            url: url.jsredirectOnCancel,
            method: 'POST',
            cache: false,
            dataType: 'json',
            data: { handshake: redirectOnCancel },
            error: function () {
                window.location.href = url.cancel;
            }
        });
    }
}

/**
 * Initiates Paysera "screen window" after checking if some variables
 * 
 * @param string hash 
 * @param string sBaseUrl 
 * @param string functionset 
 * @param bool redirectOnCancel 
 */
function payOnscreen(hash, sBaseUrl, functionset, redirectOnCancel) {
    url = {
        fail: sBaseUrl + 'onscreen/index/fail/',
        cancel: sBaseUrl + 'onscreen/index/cancel/',
        json: sBaseUrl + 'onscreen/json/fetch/',
        success: sBaseUrl + 'onscreen/index/success/',
        jsonCancel: sBaseUrl + 'onscreen/json/cancel/'
    };

    if (hash !== '') {
        if (typeof PayseraCheckout == 'undefined') {
            failedToConnect(redirectOnCancel, url, functionset);
            return;
        }
        var checkoutApp = new PayseraCheckout.App();
        /**
         * sends request to json controller
         * if it failed to connect to module it will redirect user back to cart,
         * with a message that says failed to connect
         */
        jQuery.ajax({
            url: url.json,
            method: 'POST',
            cache: false,
            dataType: 'json',
            data: { handshake: hash },
            success: function (request) {
                eval(functionset);
                checkoutApp.pay({
                    data: request['data'],
                    sign: request['sign']
                });
            },
            error: function () {
                eval(functionset);
                window.location.href = url.fail;
            }
        });

        checkoutApp.on(PayseraCheckout.AppEvent.SUCCESS, function () {
            // happens after successful payment, when window is closed
            window.location.href = url.success;
        });

        checkoutApp.on(PayseraCheckout.AppEvent.ERROR, function (message) {
            // happens after unsuccessful payment when window is closed or when buyer closes window.
            cancelOnscreen(redirectOnCancel, url);
        });
    }
}
