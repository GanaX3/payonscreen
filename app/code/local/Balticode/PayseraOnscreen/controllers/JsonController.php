<?php
class Balticode_PayseraOnscreen_JsonController extends Mage_Core_Controller_Front_Action
{
    /**
     * Gets request, trims it sends to fetchPayseraDnS function for processing.
     * If there's no $_POST data function redirect to 404
     *
     */
    public function fetchAction()
    {
        $handshake = null;
        if ($this->getRequest()->isPost()) {
            $handshake = trim(implode($this->getRequest()->getParams('handshake', null)));
        }

        $active = Mage::helper('payseraonscreen')->checkActive();
        if ($active && !empty($handshake)) {
            $internalModel = Mage::getModel('payseraonscreen/internal');
            //Processes the request and generates data and sign variables
            $request = $internalModel->buildRequest($handshake);

            //Sends data to frontend
            $this->getResponse()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody($request);
        } else {
            $this->norouteAction();
        }
    }

    /**
     * Cancels order without redirecting
     */
    public function cancelAction()
    {
        $handshake = null;
        if ($this->getRequest()->isPost()) {
            $handshake = trim(implode($this->getRequest()->getParams('handshake', null)));
        }

        $active = Mage::helper('payseraonscreen')->checkActive();
        if ($active && !empty($handshake)) {
            $internalModel = Mage::getModel('payseraonscreen/internal');
            $internalModel->cancelOrder();

            $this->getResponse()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode(array()));
        } else {
            $this->norouteAction();
        }
    }
}
