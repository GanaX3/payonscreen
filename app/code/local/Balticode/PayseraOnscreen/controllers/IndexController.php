<?php
class Balticode_PayseraOnscreen_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Cancels order and redirects to cart
     */
    public function cancelAction()
    {
        $internalModel = Mage::getModel('payseraonscreen/internal');
        $internalModel->cancelOrder();

        Mage::getSingleton('core/session')
            ->addNotice(Mage::helper('checkout')->__('Payment and order have been cancelled.'));

        $this->_redirect('checkout/cart');
    }

    /**
     * Validates payment, Deactivates quote and lands to success page
     */
    public function successAction()
    {
        if ($this->getRequest()->getParams() !== array()) {
            $requestModel = Mage::getModel('payseraonscreen/requests');
            $requestModel->payValidate();
        }

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $quote->setIsActive(false)->save();

        $this->_redirect('checkout/onepage/success');
    }

    /**
     * If connection failed the order has to be cancelled.
     * Redirect is good in this scenario to inform the issue, with translation,
     * while user is redirected to cart
     */
    public function failAction()
    {
        $internalModel = Mage::getModel('payseraonscreen/internal');
        $internalModel->cancelOrder();

        Mage::getSingleton('core/session')
            ->addNotice(Mage::helper('checkout')->__('Could not connect to the service'));

        $this->_redirect('checkout/cart');
    }
}
