<?php
class Balticode_PayseraOnscreen_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Fetches Paysera WebToPay file for loading
     *
     * @return string full path to WebToPay.php file
     */
    public function getPayseraWebToPay()
    {
        $base = Mage::getModuleDir('Model', 'Mage_Paysera');
        $base .= '/libwebtopay/WebToPay.php';
        return $base;
    }

    /**
     * Checks weather the module is active or not
     *
     * @return bool
     */
    public function checkActive()
    {
        $active = Mage::getStoreConfig('payment/payseraOnscreen/active');

        return $active === '1';
    }

    /**
     * Checks if variable is card or not
     *
     * @param string $text
     * @return bool
     */
    public function checkCard($text)
    {
        return trim($text) === 'card';
    }
}
