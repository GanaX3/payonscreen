<?php
class Balticode_PayseraOnscreen_Model_Requests extends Mage_Core_Model_Abstract
{
    /**
     * all methods in this class require this file to be loaded
     */
    public function __construct()
    {
        $wtpLocation = Mage::helper('payseraonscreen')->getPayseraWebToPay();
        include_once $wtpLocation;
    }

    /**
     * Validates payment.
     *
     * This is a copy from paysera/model function with updated logic.
     * Replaced exit('OK') with string return if this function where to be used
     * in conjunction with others
     *
     * @return string status of a payment
     * [`No such order!`, `Bad amount: $1%s Should be: $2%s`, `Bad currency: %s`]
     */
    public function payValidate()
    {
        $pData = Mage::getStoreConfig('payment/paysera');
        $request = Mage::app()->getRequest()->getParams();

        //sends response for Paysera to decrypt
        $response = WebToPay::checkResponse($request, array(
            'projectid'     => $pData['api_key'],
            'sign_password' => $pData['api_secret'],
        ));

        $status = 'No such order!';
        $error = false;
        //Verifies information and sets $status
        if ($response['status'] == 1) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($response['orderid']);
            if ($order->getIncrementId()) {
                //fetching and formating data for it to be compared
                $responseAmount = intval(number_format($response['amount'], 0, '', ''));
                $systemAmount   = intval(number_format(($order->getGrandTotal() * 100), 0, '', ''));

                if ($responseAmount < $systemAmount) {
                    $status = 'Bad amount: ' . $responseAmount . ' Should be: ' . $systemAmount;
                    $error = true;
                }
                if ($response['currency'] != $order->getOrderCurrencyCode()) {
                    $status = 'Bad currency: ' . $response['currency'];
                    $error = true;
                }
                if (!$error) {
                    try {
                        $order->sendNewOrderEmail();
                        $order->setStatus(Mage_Sales_Model_Order::STATE_PROCESSING)->save();
                        $status = 'OK';
                    } catch (Exception $e) {
                        $status = get_class($e) . ':' . $e->getMessage();
                    }
                }
            }
        }
        return $status;
    }

    /**
     * Creates data and sign variables for an order
     * to be pocessed via Paysera payment method
     *
     * This is a copy from Mage/Paysera/Model with minor modification
     * to fit better for this scenario
     *
     * @param int $incId      increment id
     *
     * @return array         data and sign variables in an array
     */
    public function generateDataSign($incId)
    {
        $order = Mage::getModel('sales/order')->loadByIncrementId($incId);
        $active = Mage::helper('payseraonscreen')->checkActive();
        if ($active && $order->getIncrementId()) {
            $language = substr(Mage::app()->getLocale()->getLocaleCode(), 3, 5);

            $payment = $order->getPayment();
            $mokBud = $payment->getAdditionalInformation('mok_bud');
            $pData = Mage::getStoreConfig('payment/paysera');

            $lng = array(
                'LT' => 'LIT',
                'LV' => 'LAV',
                'EE' => 'EST',
                'RU' => 'RUS',
                'DE' => 'GER',
                'PL' => 'POL'
            );

            $request = null;
            try {
                $request = WebToPay::buildRequest(array(
                    'projectid'     => $pData['api_key'],
                    'sign_password' => $pData['api_secret'],

                    'orderid'       => $order->getIncrementId(),
                    'amount'        => intval(number_format(($order->grand_total * 100), 0, '', '')),
                    'currency'      => $order->order_currency_code,
                    'lang'          => (isset($lng[$language]) ? $lng[$language] : 'ENG'),
                    'frame'         => 1,

                    'accepturl'     => Mage::getUrl('onscreen/index/success'),
                    'cancelurl'     => Mage::getUrl('onscreen/index/cancel'),
                    'callbackurl'   => Mage::getUrl('paysera/pay/callback'),
                    'payment'       => $mokBud,
                    'country'       => $order->getShippingAddress()->getCountry(),

                    'p_firstname'   => $order->getBillingAddress()->getFirstname(),
                    'p_lastname'    => $order->getBillingAddress()->getLastname(),
                    'p_email'       => $order->getCustomerEmail(),
                    'p_state'       => '',

                    'test'          => $pData['test'],
                ));
            } catch (WebToPayException $e) {
                $request = get_class($e) . ': ' . $e->getMessage();
            }
        }
        return $request;
    }
}
