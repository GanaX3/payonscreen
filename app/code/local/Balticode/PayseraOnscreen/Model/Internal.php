<?php
class Balticode_PayseraOnscreen_Model_Internal extends Mage_Core_Model_Abstract
{
    /**
     * Cancels order and resets reserved order id
     */
    public function cancelOrder()
    {
        //geting quote and reserved orderid
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $reservedId = $quote->getReservedOrderId();

        //getting order an canceling it
        $order = Mage::getModel('sales/order')->loadByIncrementId($reservedId);
        if ($order->canCancel()) {
            try {
                $order->cancel();
                $order->getStatusHistoryCollection(true);
                $order->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        $quote->setReservedOrderId(null);
    }

    /**
     * Prepairs data for request generation and sends it to requests model to proccess
     *
     * @param string $encodedData base64 encoded increment id
     * @return string json encoded response 
     */
    public function buildRequest($encodedData)
    {
        $requestModel = Mage::getModel('payseraonscreen/requests');
        $encodedData = intval(base64_decode($encodedData));
        $response = $requestModel->generateDataSign($encodedData);
        return json_encode($response);
    }
}
